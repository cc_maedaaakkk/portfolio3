﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace portfolio3
{
    class Ruler
    {
        private int id;
        private string name;
        private string unit;

        public Ruler(int id, string name, string unit)
        {
            this.id = id;
            this.name = name;
            this.unit = unit;
        }

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Unit { get => unit; set => unit = value; }
    }       
}
