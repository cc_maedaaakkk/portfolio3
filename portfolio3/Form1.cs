﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace portfolio3
{
    public partial class Form1 : Form
    {
       
        List<Ruler> rulers = new List<Ruler>();

        string InputNumString;
        bool select;
        
        public Form1()
        {
            InitializeComponent();
        }

        public void LoadRulerList()
        {
            
            rulers.Add(new Ruler(0,"年","年"));
            rulers.Add(new Ruler(1,"日","日"));
            rulers.Add(new Ruler(2,"時間","時間"));

            foreach(Ruler r in rulers)
            {
                this.rulerList1.Items.Add(r.Name);
                this.rulerList2.Items.Add(r.Name);
            }
        }

    
       private void ResetRulerInfo()
        {
            InputNumString = "0";
            calcNumText1.Text = InputNumString;
            calcNumText2.Text = InputNumString;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ResetRulerInfo();
            LoadRulerList();
            
        }

        private void NumBtnClick(object sender, EventArgs e)
        {
            
            Button btn = (Button)sender;

            string btnText = btn.Text;
            if (InputNumString == "0")//初期化数値０の上書き
            {
                InputNumString = btnText;
                if (select)
                {
                    calcNumText1.Text = InputNumString;
                }
                
            }
            else//入力の数値反映
            {
                InputNumString += btnText;
                if (select)
                {
                    calcNumText1.Text = InputNumString;
                }
                else
                {
                    calcNumText2.Text = InputNumString;
                }
            }

            int inputNum = int.Parse(InputNumString);
            float result = 0;
            if (this.rulerList1.SelectedIndex == 0)//ルーラーリスト１が年ならば
            {
                switch (this.rulerList2.SelectedIndex)//ルーラーリスト２
                {
                    case 0://年
                        result = inputNum * 1f;
                        break;
                    case 1://日
                        result = inputNum * 365f;
                        break;
                    case 2://時間
                        result = inputNum * 24f * 365f;
                        break;
                }
            }
            else if (this.rulerList1.SelectedIndex == 1)//ルーラーリスト１が日ならば
            {
                switch (this.rulerList2.SelectedIndex)//ルーラーリスト２
                {
                    case 0://年
                        result = inputNum / 365f;
                       
                        break;
                    case 1://日
                        result = inputNum * 1f;
                        break;
                    case 2://時間
                        result = inputNum * 24f ;
                        break;
                }
            }
            else if (this.rulerList1.SelectedIndex == 2)//ルーラーリスト１が時間ならば
            {
                switch (this.rulerList2.SelectedIndex)//ルーラーリスト２
                {
                    case 0://年
                        result = inputNum / 365f /24f;
                        break;
                    case 1://日
                        result = inputNum  / 24f;
                        break;
                    case 2://時間
                        result = inputNum * 1f;
                        break;
                }

            }

            if (select)
            {
                this.calcNumText2.Text = result.ToString();
                if (result % 1 == 0)
                {
                    this.aboutCalc.Text = "";
                    
                }
                else
                {
                    this.aboutCalc.Text = "約";
                }
            }
            else
            {
                this.calcNumText2.Text = "0";
                this.attention.Text = "↓数字をクリックしてね";//入力判定対応
            }
        }


        private void clearBtnClick(object sender, EventArgs e)
        {
            ResetRulerInfo();
        }


        private void RulerListChanged1(object sender, EventArgs e)
        {
            ResetRulerInfo();
            int index = this.rulerList1.SelectedIndex;
        }


        private void RulerListChanged2(object sender, EventArgs e)
        {
            ResetRulerInfo();      
        }


        private void calcnumTexr1Click(object sender, EventArgs e)
        {
            select = true;
            ResetRulerInfo();
            this.attention.Text = "";
        }

        private void aboutCalcClick(object sender, EventArgs e)
        {

        }

        private void calcNumText2Click(object sender, EventArgs e)
        {

        }
    }
}
